import { Component, OnInit, Input } from '@angular/core';
import { Router } from "@angular/router";
import { UserService } from "../../services/User/user.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  user:any;
  constructor(private router: Router, private userService: UserService) {
    this.user = userService.getUser();
  }




  ngOnInit() {
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['\login']);
  }

}
