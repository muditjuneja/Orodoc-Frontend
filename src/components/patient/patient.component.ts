import { Component, OnInit } from '@angular/core';
import { UserService } from "../../services/User/user.service";

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit {

  requests = [];
  constructor(private userService: UserService) {

  }

  async ngOnInit() {
    await this.getAllPendingRequests();
  }


  async getAllPendingRequests() {
    try {
      // console.log('Sending');
      let response = await this.userService.getRequests(1, 'pending').toPromise();
      if (response['valid']) {
        // console.log(response);
        this.requests = response['data'];

      } else {
        console.error(response);
      }
    } catch (error) {
      console.log(error);
    }
  }


  async approve(request) {
    try {
      // console.log('Sending');
      let response = await this.userService.updateRequest(request, 'approved').toPromise();
      if (response['valid']) {
        // console.log(response);
        alert('Approved');
        await this.getAllPendingRequests();
      } else {
        console.error(response);
      }
    } catch (error) {
      console.log(error);
    }
  }


  async reject(request) {
    try {
      // console.log('Sending');
      let response = await this.userService.updateRequest(request, 'denied').toPromise();
      if (response['valid']) {
        // console.log(response);
        alert('Rejected');
        await this.getAllPendingRequests();
      } else {
        console.error(response);
      }
    } catch (error) {
      console.log(error);
    }
  }




}
