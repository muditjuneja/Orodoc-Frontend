import { Component, OnInit } from '@angular/core';
import { UserService } from "../../services/User/user.service";

@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
  styleUrls: ['./doctor.component.css']
})
export class DoctorComponent implements OnInit {

  users = [];
  pending_requests = [];
  approved_requests = [];
  constructor(private userService: UserService) {

  }

  async ngOnInit() {
    try {

      let response = await Promise.all([this.userService.getUsers(1).toPromise(), this.userService.getRequests(2, 'pending').toPromise(), this.userService.getRequests(2, 'approved').toPromise()]);
      let users = [];
      if (response[0]['valid']) {
        users = response[0]['data'];
      } else {
        console.error(response);
      }

      if (response[1]['valid']) {
        this.pending_requests = response[1]['data'];
        for (let request of this.pending_requests) {
          let index = users.findIndex(x => x.id == request.requested_for.id);
          if (index > -1) {
            users.splice(index, 1);
          }
        }
      } else {
        console.error(response);
      }


      if (response[2]['valid']) {
        this.approved_requests = response[2]['data'];
        for (let request of this.approved_requests) {
          let index = users.findIndex(x => x.id == request.requested_for.id);
          if (index > -1) {
            users.splice(index, 1);
          }
        }
      } else {
        console.error(response);
      }
      this.users = users;
    } catch (error) {
      console.error(error);
    }
  }



  async requestRecord(user) {
    try {
      let response = await this.userService.requestRecord(user.id).toPromise();
      if (response['valid']) {
        let index = this.users.findIndex(x => x.id == user.id);
        if (index > -1) {
          this.users.splice(index, 1);
        }
        alert('Requested');
        let pending_requests_response = await this.userService.getRequests(2, 'pending').toPromise();
        if (pending_requests_response['valid']) {
          this.pending_requests = pending_requests_response['data'];
        }
      } else {
        console.error(response);
      }
    } catch (error) {
      console.error(error);
    }
  }


  async viewRecord() {
    alert('Viewed');
  }

}
