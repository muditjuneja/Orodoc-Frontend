import { Component, OnInit } from '@angular/core';
import { UserService } from "../../services/User/user.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) { }
  error_message: string;
  loader = false;
  ngOnInit() {
  }


  async login(form) {
    this.error_message = "";
    this.loader = true;
    let data = form.value;
    try {
      let response = await this.userService.login(data).toPromise();
      if (response['valid']) {
        if (response['data']) {
          localStorage.setItem('user', JSON.stringify(response['data']['user']));
          localStorage.setItem('token', response['data']['token']);
        }
        // console.log(response['data']);
        if (response['data']['user']['role'] == 1) {
          this.router.navigate(['/patients'])
        } else if (response['data']['user']['role'] == 2) {
          this.router.navigate(['/doctors'])
        } else {
          this.router.navigate(['/pharmacists'])
        }

      } else {
        this.error_message = response['message'];
      }
    } catch (error) {
      this.error_message = 'An error has occured.';
    }
    this.loader = false;

  }

}
