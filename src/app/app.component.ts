import { Component } from '@angular/core';
import { Router } from "@angular/router";
// import { NgForm } from "@angular/forms/src/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(private router: Router) {

  }
  ngOnInit() {
    let user = JSON.parse(localStorage.getItem('user'));
    let token = localStorage.getItem('token');
    if (user && token) {
      if (user.role == 1) {
        this.router.navigate(['/patients'])
      } else if (user.role == 2) {
        this.router.navigate(['/doctors'])
      } else {
        this.router.navigate(['/pharmacists'])
      }
    } else {
      this.router.navigate(['/login']);
    }

  }
}
