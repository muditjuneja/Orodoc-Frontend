import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router'
import { HttpClientModule } from '@angular/common/http'


import { AppComponent } from './app.component';
import { PatientComponent } from "../components/patient/patient.component";
import { DoctorComponent } from "../components/doctor/doctor.component";
import { PharmacistComponent } from "../components/pharmacist/pharmacist.component";
import { LoginComponent } from "../components/login/login.component";
import { UserService } from "../services/User/user.service";
import { HeaderComponent } from "../components/header/header.component";
import { SignupComponent } from '../components/signup/signup.component';




const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'patients', component: PatientComponent },
  { path: 'doctors', component: DoctorComponent },
  { path: 'pharmacists', component: PharmacistComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    PatientComponent,
    DoctorComponent,
    PharmacistComponent,
    LoginComponent,
    HeaderComponent,
    SignupComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    RouterModule.forRoot(routes)
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
