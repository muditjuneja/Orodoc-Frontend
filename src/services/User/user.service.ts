import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment'
import { Router } from "@angular/router";
import { HttpHeaders } from "@angular/common/http";

@Injectable()
export class UserService {
  base_uri: string = environment.base_uri;
  user: any;
  constructor(private httpClient: HttpClient, private router: Router) {
    if (localStorage.getItem('user')) {
      this.user = JSON.parse(localStorage.getItem('user'));
    } else {
      router.navigate(['/login']);
    }

  }


  getUser() {
    let user;
    if (localStorage.getItem('user')) {
      user = JSON.parse(localStorage.getItem('user'));
    }
    return user;

  }

  getToken() {
    return localStorage.getItem('token');
  }


  signup(data) {
    return this.httpClient.post(this.base_uri + '/user/create', data);
  }

  login(data) {
    return this.httpClient.post(this.base_uri + '/user/login', data);
  }

  getUsers(role) {
    return this.httpClient.get(this.base_uri + '/users/' + role, {
      headers: new HttpHeaders().set('Authorization', 'Bearer ' + this.getToken())
    }
    );
  }


  requestRecord(user_id) {
    this.user = JSON.parse(localStorage.getItem('user'));
    let data = { 'requested_for': user_id, 'requested_by': this.user.id };
    return this.httpClient.post(this.base_uri + '/user/request', data, {
      headers: new HttpHeaders().set('Authorization', 'Bearer ' + this.getToken())
    });
  }

  getRequests(type, status) {
    if (!status) {
      status = 'pending';
    }
    // console.log(this.getToken());
    this.user = JSON.parse(localStorage.getItem('user'));
    return this.httpClient.get(this.base_uri + '/user/requests/' + this.user.id + '/' + type + '/' + status, {
      headers: new HttpHeaders().set('Authorization', 'Bearer ' + this.getToken())
    });
  }


  updateRequest(request, status) {
    let data = { 'status': status };
    return this.httpClient.put(this.base_uri + '/user/request/' + request.id, data, {
      headers: new HttpHeaders().set('Authorization', 'Bearer ' + this.getToken())
    });
  }

}
